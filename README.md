リポジトリからクローンする
- git clone git@gitlab.com:kazufumi.miura/wordpress.git

Docker Composeの環境変数を設定する
- cp .env.template .env
- .envファイルをエディタで開いて好きなポート番号を設定する（例：8000）

コンテナを立ち上げる
- docker compose up -d

localhost:xxxx（設定したポート番号）をブラウザに入力し、Wordpressの初期設定を行う。
